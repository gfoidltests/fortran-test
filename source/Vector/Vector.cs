﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;

namespace Vector
{
    public class Vector : IEnumerable<double>
    {
        private readonly double[] _vector;
        //---------------------------------------------------------------------
        public int Size => _vector.Length;
        //---------------------------------------------------------------------
        public Vector(int size)        => _vector = new double[size];
        public Vector(double[] vector) => _vector = vector;
        //--------------------------------------------------------------------- 
        public ref double this[int index] => ref _vector[index];
        //---------------------------------------------------------------------
        public static Vector Add(Vector a, Vector b)
        {
            int size = a.Size;
            var c    = new Vector(size);
            Native.__vector_MOD_add(a._vector, b._vector, c._vector, ref size);

            return c;
        }
        //---------------------------------------------------------------------
        public static Vector operator +(Vector a, Vector b) => Add(a, b);
        //---------------------------------------------------------------------
        public double Dot(Vector b)
        {
            int size = this.Size;
            return Native.__vector_MOD_dot(_vector, b._vector, ref size);
        }
        //---------------------------------------------------------------------
        public IEnumerator<double> GetEnumerator() => _vector.AsEnumerable().GetEnumerator();
        IEnumerator IEnumerable.GetEnumerator()    => this.GetEnumerator();
        //---------------------------------------------------------------------
        // For collection initializer
        private int _set = 0;
        public void Add(double d) => _vector[_set++] = d;
        //---------------------------------------------------------------------
        private static class Native
        {
            [DllImport("vector_ftn")]
            public static extern void __vector_MOD_add(double[] a, double[] b, double[] c, ref int size);

            [DllImport("vector_ftn")]
            public static extern double __vector_MOD_dot(double[] a, double[] b, ref int size);
        }
    }
}
