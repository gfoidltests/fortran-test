module vector
    implicit none
    private
    public add, dot
    !--------------------------------------------------------------------------
    integer, parameter  :: dp = kind(0.d0)
    contains
    !--------------------------------------------------------------------------
    subroutine add(a, b, c, n)
        integer, intent(in)                 :: n
        real(dp), intent(in), dimension(n)  :: a, b
        real(dp), intent(out), dimension(n) :: c
        
        c = a + b
    end subroutine add
    !--------------------------------------------------------------------------
    function dot(a, b, n) result (d)
        integer, intent(in)                 :: n
        real(dp), intent(in), dimension(n)  :: a, b
        real(dp)                            :: d
        
        d = dot_product(a, b)
    end function dot
end module vector