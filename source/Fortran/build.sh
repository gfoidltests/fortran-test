#!/bin/bash

build() {
    name=
    if [[ "$1" == "win" ]]; then
        name="vector_ftn.dll"
    else
        name="libvector_ftn.so"
    fi

    options="-Wall -Wno-tabs -Wextra -Wimplicit-interface -fPIC -fmax-errors=1 -O3 -march=native -floop-parallelize-all -ffast-math -funroll-loops"

    mkdir -p ./bin
    gfortran $options -shared -o "./bin/$name" vector.f95
}

if [[ $# -lt 1 ]]; then
    echo "win or linux as platform must be given as first argument"
    exit 1
fi

build $@
