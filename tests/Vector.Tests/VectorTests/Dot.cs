﻿using NUnit.Framework;

namespace Vector.Tests.VectorTests
{
    [TestFixture]
    public class Dot
    {
        [Test]
        public void Vectors_given___correct_dotproduct()
        {
            var a = new Vector(3) { 1, 2, 3 };
            var b = new Vector(3) { 4, 5, 6 };

            double actual = a.Dot(b);

            Assert.AreEqual(32, actual, 1e-6);
        }
        //---------------------------------------------------------------------
        [Test]
        public void Reverse_dotproduct___same_value()
        {
            var a = new Vector(3) { 1, 2, 3 };
            var b = new Vector(3) { 4, 5, 6 };

            double dot1 = a.Dot(b);
            double dot2 = b.Dot(a);

            Assert.AreEqual(dot1, dot2, 1e-6);
        }
    }
}