﻿using NUnit.Framework;

namespace Vector.Tests.VectorTests
{
    [TestFixture]
    public class Add
    {
        [Test]
        public void Vectors_given___correct_summed_vector()
        {
            var a = new Vector(3) { 1, 2, 3 };
            var b = new Vector(3) { 4, 5, 6 };

            Vector actual = a + b;

            double[] expected = { 5, 7, 9 };

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}