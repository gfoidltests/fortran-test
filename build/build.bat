@echo off

mkdir c:\projects\fortran-test\source\Fortran\bin
powershell -Command "Invoke-WebRequest https://bitbucket.org/gfoidltests/fortran-test/downloads/vector_ftn.dll -OutFile c:\projects\fortran-test\source\Fortran\bin\vector_ftn.dll"
powershell -Command "Invoke-WebRequest https://bitbucket.org/gfoidltests/fortran-test/downloads/libvector_ftn.so -OutFile c:\projects\fortran-test\source\Fortran\bin\libvector_ftn.so"

cd source\Vector
dotnet restore
dotnet build -c Release
dotnet pack -o ..\..\NuGet-Packed -c Release --no-build
cd ..\..
dotnet restore
dotnet build -c Release

dotnet test -c Release --no-build tests\Vector.Tests\Vector.Tests.csproj
dotnet test -c Release --no-build tests\Vector.Tests.NuGet\Vector.Tests.NuGet.csproj

echo on