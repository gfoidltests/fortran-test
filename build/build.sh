#!/bin/bash

set -e

cd source/Fortran
chmod +x build.sh
./build.sh linux
# Fake the win-build here
cp bin/libvector_ftn.so bin/vector_ftn.dll
cd ../Vector
dotnet restore
dotnet build -c Release
dotnet pack -o ../../NuGet-Packed -c Release --no-build
cd ../..
dotnet restore
dotnet build -c Release
find tests -name *.csproj | xargs -n1 dotnet test --no-build -c Release
